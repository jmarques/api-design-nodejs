# API Design in Node.js & Secure Authentication using JWTs
> João Marques

Design and build APIs in Node.js from the ground up using Express. You'll learn how to plan your routes to match REST verb methods using the framework routers and controllers. Finally, you'll add authentication middleware with JWTs to properly restrict access to the protected parts of the API.

- [Slides](https://slides.com/jgmarques/api-design-nodejs)
- [Resources](#resources)
- [Course](#course)
- [Exercises](#exercises)

## Resources
* [Nodejs](https://nodejs.org/en/)
* [Express](https://expressjs.com/)
* [Babel](https://babeljs.io/)
* [ESLint](https://eslint.org/)
* [Nodemon](https://nodemon.io/)
* [Husky](https://github.com/typicode/husky)

## Course
This workshop consists of a series of exercices that are supported and explained using the slides. Each exercise has a starting branch and solution branch. Example `exercise-1` and `exercise-1-solution`.

## Exercises

To start the exercises, clone the [repository](https://gitlab.com/jmarques/api-design-nodejs.git)

### Hello World Express
* Checkout branch `exercise-1`

In this exercise you'll be creating a simple Express based API in node that returns "Hello World" and logs the requests.

- [ ] install dependencies with npm
- [ ] create a route named 'api' that sends an 'Hello World' response
- [ ] log each call to the console using the morgan logger
- [ ] start the server and test it

### Multiple Routes to access data
* Checkout branch `exercise-2`

In this exercise you'll be creating two different routes to access two different sets of data: posts and users

- [ ] create a route named 'api/users' that returns a list of users, and a route named 'api/users/:username' to get a specific user. Make use of the `resources/users` file to access the list of users
- [ ] create a route named 'api/posts' that returns a list of posts, and a route named 'api/posts/:id' to get a specific post. Make use of the `resources/posts` file to access the list of posts
- [ ] start the server and test it

### Creating Routers to handle CRUD methods
* Checkout branch `exercise-3`

In this exercise you'll be expanding on the previous exercise to implement a full CRUD on two different sets of data: posts and users

- [ ] create a router named 'api/users' that handles all the methods available in `resources/users`
- [ ] create a router named 'api/posts' that handles all the methods available in `resources/posts`
- [ ] abstract the 'api' in its own router, using the routers created above
- [ ] start the server and test it
- [ ] BONUS: in REST methodology, in a POST response, you should return the URL to the newly created resource in the Location header in the response. Implement that in your controllers

NOTE: you will need more middleware to be able to send JSON to your server. Install the package `body-parser` to do so, and use it in your middleware file.

### Securing the application with JWT authentication
* Checkout branch `exercise-4`

In this exercise you'll be securing the application with JWT.

- [ ] create a new route named '/login' that allows someone to login and obtain a token
- [ ] lock down all routes with the exception of creating a new user (signup)
- [ ] start the server and test it
- [ ] BONUS: make sure that editing or removing a user is done only if it is the user itself performing the request

NOTE: you will need three packages in this exercise: `jsonwebtoken`, `express-jwt`, and `bcrypt`. Install them with npm
