import { Router } from 'express'
import { getAll, getById, create, update, remove, removeAll } from './controller'

const router = Router()

router.route('/')
  .get(getAll)
  .post(create)
  .delete(removeAll)

router.route('/:id')
  .get(getById)
  .patch(update)
  .delete(remove)

export default router
