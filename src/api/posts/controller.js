import * as postsModel from '../../resources/posts'

export const getAll = (req, res) => {
  const posts = postsModel.getAll()
  res.status(200).json(posts)
}

export const create = (req, res) => {
  const post = req.body
  if (post && post.content && post.author && post.tags) {
    const postId = postsModel.create(post.content, post.author, post.tags)
    res.status(200).location(`${req.baseUrl}/${postId}`).end(postId)
  } else {
    res.status(400).end()
  }
}

export const removeAll = (req, res) => {
  postsModel.removeAll()
  res.status(200).end()
}

export const getById = (req, res) => {
  const postId = req.params.id
  const post = postsModel.getById(postId)
  if (post) {
    res.status(200).json(post)
  } else {
    res.status(404).end()
  }
}

export const update = (req, res) => {
  const postId = req.params.id
  const newPost = req.body
  const post = postsModel.getById(postId)
  if (post) {
    if (post && newPost.content && newPost.tags) {
      postsModel.update(postId, newPost.content, newPost.tags)
      res.status(200).end()
    } else {
      res.status(400).end()
    }
  } else {
    res.status(404).end()
  }
}

export const remove = (req, res) => {
  const postId = req.params.id
  const post = postsModel.getById(postId)
  if (post) {
    postsModel.remove(postId)
    res.status(200).json(post)
  } else {
    res.status(404).end()
  }
}
