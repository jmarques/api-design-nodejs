import { Router } from 'express'
import { getAll, getByUsername, create, update, remove, removeAll } from './controller'

const router = Router()

router.route('/')
  .get(getAll)
  .post(create)
  .delete(removeAll)

router.route('/:username')
  .get(getByUsername)
  .patch(update)
  .delete(remove)

export default router
