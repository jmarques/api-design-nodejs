import * as usersModel from '../../resources/users'

export const getAll = (req, res) => {
  const users = usersModel.getAll()
  res.status(200).json(users)
}

export const create = (req, res) => {
  const user = req.body
  if (user && user.username && user.name && user.email && user.password) {
    try {
      usersModel.create(user.username, user.name, user.email, user.password)
      res.status(200).location(`${req.baseUrl}/${user.username}`).end()
    } catch (e) {
      res.status(500).send(`User with username ${user.username} already exists`)
    }
  } else {
    res.status(400).end()
  }
}

export const removeAll = (req, res) => {
  usersModel.removeAll()
  res.status(200).end()
}

export const getByUsername = (req, res) => {
  const username = req.params.username
  const user = usersModel.getByUsername(username)
  if (user) {
    res.status(200).json(user)
  } else {
    res.status(404).end()
  }
}

export const update = (req, res) => {
  const loggedUsername = req.user.username
  const username = req.params.username
  if (loggedUsername === username) {
    const newUser = req.body
    const user = usersModel.getByUsername(username)
    if (user) {
      if (user && user.name && user.email) {
        usersModel.update(username, newUser.name, newUser.email)
        res.status(200).end()
      } else {
        res.status(400).end()
      }
    } else {
      res.status(404).end()
    }
  } else {
    res.status(403).end()
  }
}

export const remove = (req, res) => {
  const loggedUsername = req.user.username
  const username = req.params.username
  if (loggedUsername === username) {
    const user = usersModel.getByUsername(username)
    if (user) {
      usersModel.remove(username)
      res.status(200).json(user)
    } else {
      res.status(404).end()
    }
  } else {
    res.status(403).end()
  }
}
