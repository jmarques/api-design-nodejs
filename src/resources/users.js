import fs from 'fs'
import path from 'path'
import bcrypt from 'bcrypt'

const getUsersFromFile = () => {
  const filePath = path.resolve(__dirname, 'users.json')
  return JSON.parse(fs.readFileSync(filePath, 'utf8'))
}

const writeUsersToFile = (users) => {
  const filePath = path.resolve(__dirname, 'users.json')
  fs.writeFileSync(filePath, JSON.stringify(users, null, 2))
}

export const getAll = () => {
  const users = getUsersFromFile()
  users.forEach(item => {
    delete item.password
  })
  return users
}

export const getByUsername = (username) => {
  const users = getUsersFromFile()
  const user = users.find(item => item.username === username)
  if (user) {
    const passwordHash = user.password
    delete user.password
    user.checkPassword = password => bcrypt.compareSync(password, passwordHash)
  }
  return user
}

export const create = (username, name, email, password) => {
  const user = getByUsername(username)
  if (user) {
    throw new Error(`User with username ${username} already exists`)
  }
  const users = getUsersFromFile()
  users.push({
    username,
    name,
    email,
    password: bcrypt.hashSync(password, 8)
  })
  writeUsersToFile(users)
}

export const update = (username, newName, newEmail) => {
  const users = getUsersFromFile()
  let found = false
  for (let i = 0; i < users.length; i++) {
    const item = users[i]
    if (item.username === username) {
      found = true
      item.name = newName
      item.email = newEmail
      break
    }
  }
  if (!found) {
    throw new Error(`Username ${username} does not exist`)
  }
  writeUsersToFile(users)
}

export const remove = (username) => {
  const users = getUsersFromFile()
  writeUsersToFile(users.filter(item => item.username !== username))
}

export const removeAll = () => {
  writeUsersToFile([])
}
