import fs from 'fs'
import path from 'path'
import uuidV1 from 'uuid/v1'

const getPostsFromFile = () => {
  const filePath = path.resolve(__dirname, 'posts.json')
  return JSON.parse(fs.readFileSync(filePath, 'utf8'))
}

const writePostsToFile = (users) => {
  const filePath = path.resolve(__dirname, 'posts.json')
  fs.writeFileSync(filePath, JSON.stringify(users, null, 2))
}

export const getAll = () => {
  return getPostsFromFile()
}

export const getById = (id) => {
  const posts = getPostsFromFile()
  return posts.find(item => item.id === id)
}

export const create = (content, author, tags) => {
  const posts = getPostsFromFile()
  const id = uuidV1()
  posts.push({
    id,
    content,
    author,
    tags
  })
  writePostsToFile(posts)
  return id
}

export const update = (id, newContent, newTags) => {
  const posts = getPostsFromFile()
  let found = false
  for (let i = 0; i < posts.length; i++) {
    const item = posts[i]
    if (item.id === id) {
      found = true
      item.content = newContent
      item.tags = newTags
      break
    }
  }
  if (!found) {
    throw new Error(`Post with id ${id} does not exist`)
  }
  writePostsToFile(posts)
}

export const remove = (id) => {
  const posts = getPostsFromFile()
  writePostsToFile(posts.filter(item => item.id !== id))
}

export const removeAll = () => {
  writePostsToFile([])
}
