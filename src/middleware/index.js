import morgan from 'morgan'
import bodyParser from 'body-parser'
import expressJwt from 'express-jwt'
import config from '../config'

export default app => {
  app.use(morgan('dev'))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use('/api', expressJwt({ secret: config.jwtSecret }).unless({ path: [{ url: '/api/users', methods: ['POST'] }] }))

  app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
      res.status(401).send('invalid token...')
    }
  })
}
