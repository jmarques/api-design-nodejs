const env = process.env.NODE_ENV || 'development'

export default {
  env,
  port: 3000,
  jwtSecret: 'my secret'
}
