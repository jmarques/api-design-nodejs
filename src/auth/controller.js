import jwt from 'jsonwebtoken'
import * as usersModel from '../resources/users'
import config from '../config'

const newToken = username => {
  return jwt.sign({ username }, config.jwtSecret)
}

export const login = (req, res) => {
  const invalid = { message: 'Incorrect username or password' }
  const body = req.body
  if (body && body.username && body.password) {
    const user = usersModel.getByUsername(body.username)
    if (user) {
      if (user.checkPassword(body.password)) {
        const token = newToken(user.username)
        return res.status(201).send({ token })
      } else {
        res.status(401).json(invalid)
      }
    } else {
      res.status(401).json(invalid)
    }
  } else {
    res.status(401).json(invalid)
  }
}
