import express from 'express'
import middleware from './middleware'
import apiRouter from './api'
import authRouter from './auth'

const app = express()

middleware(app)

app.use('/', authRouter)
app.use('/api', apiRouter)

export default app
