import app from './server'
import config from './config'

app.listen(config.port, () => {
  console.log(`REST API on http://localhost:${config.port}/api`)
})
